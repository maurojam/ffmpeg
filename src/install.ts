import * as http from 'http';
import * as fs from 'fs';
import * as path from 'path';
import * as AdmZip from 'adm-zip';
import * as mkdirp from 'mkdirp';
import {settings} from './config';

export function install(dir?: string) {
    if (dir) settings.binpath = dir;
    log('installing into: ' + settings.binpath);
    mkdirp.sync(settings.binpath);
    download(link(), zipfile(), extractor());
}

function log(...msg: any[]) {
    console.log('[ffmpeg]', '[' + settings.platform + ']', ...msg);
}

const extractors: any = {
    'win32': extractWin32,
    'darwin': extractDarwin,
}

function link() {
    return settings[settings.platform];
}

function zipfile() {
    return path.join(settings.binpath, 'temp.zip');
}

function extractor() {
    return extractors[settings.platform];
}

function download(url: string, dest: string, cb: (err?: Error) => void) {
    const file = fs.createWriteStream(dest);
    log('downloading...');
    http.get(url, (response: any) => {
        log('reading downloaded file');
        response.pipe(file);
        file.on('finish', () => {
            file.close();
            cb();
        });
    }).on('error', (err) => {
        remove(dest);
        log('error while downloading');
        if (cb) cb(err);
    });
}

function remove(path: string) {
    if (fs.existsSync(path)) fs.unlinkSync(path);
}

function extractDarwin() {
    log('extracting executable binary');
    const zip = new AdmZip(zipfile());
    zip.extractEntryTo('ffmpeg', settings.binpath, false, true);
    fs.chmodSync(path.join(settings.binpath, 'ffmpeg'), '777');
    remove(zipfile());
    log('done');
}

function extractWin32() {
    log('extracting executable binary');
    const zip = new AdmZip(zipfile());
    const entries = zip.getEntries();
    const entry = entries[0].entryName;
    zip.extractEntryTo(entry + 'bin/ffmpeg.exe', settings.binpath, false, true);
    fs.chmodSync(path.join(settings.binpath, 'ffmpeg.exe'), '777');
    remove(zipfile());
    log('done');
}




