import * as execa from 'execa';
import * as path from 'path';
import {bin} from './config';

export interface RunResponse {
    cmd: string;
    code: number;
    failed: boolean;
    killed: boolean;
    signal: string | null;
    stderr: string;
    stdout: string;
    timedOut: boolean;
}

export function run(cmd: string): Promise<RunResponse> {
    return execa.shell(`${bin()} ${cmd}`);
}