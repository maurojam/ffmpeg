import * as http from 'http';
import * as fs from 'fs';
import * as path from 'path';
import * as AdmZip from 'adm-zip';
import * as mkdirp from 'mkdirp';

export const settings: any = {
    binpath:    'bin',
    platform:   platform(),
    win32:     'http://ffmpeg.zeranoe.com/builds/win32/static/ffmpeg-20170615-bc40674-win32-static.zip',
    darwin:    'http://www.ffmpegmac.net/resources/Lion_Mountain_Lion_Mavericks_Yosemite_El-Captain_15.05.2017.zip',
}

const executables: any = {
    'win32': 'ffmpeg.exe',
    'darwin': 'ffmpeg'
}

export function platform() {
    return process.platform;
}

export function setup(binpath: string) {
    settings.binpath = binpath;
}

export function bin() {
    const exe = path.join(settings.binpath, executables[settings.platform]);
    return exe;
}





