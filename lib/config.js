"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
var path = require("path");
exports.settings = {
    binpath: 'bin',
    platform: platform(),
    win32: 'http://ffmpeg.zeranoe.com/builds/win32/static/ffmpeg-20170615-bc40674-win32-static.zip',
    darwin: 'http://www.ffmpegmac.net/resources/Lion_Mountain_Lion_Mavericks_Yosemite_El-Captain_15.05.2017.zip',
};
var executables = {
    'win32': 'ffmpeg.exe',
    'darwin': 'ffmpeg'
};
function platform() {
    return process.platform;
}
exports.platform = platform;
function setup(binpath) {
    exports.settings.binpath = binpath;
}
exports.setup = setup;
function bin() {
    var exe = path.join(exports.settings.binpath, executables[exports.settings.platform]);
    return exe;
}
exports.bin = bin;
