"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
var execa = require("execa");
var config_1 = require("./config");
function run(cmd) {
    return execa.shell(config_1.bin() + " " + cmd);
}
exports.run = run;
