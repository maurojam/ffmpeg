"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
var http = require("http");
var fs = require("fs");
var path = require("path");
var AdmZip = require("adm-zip");
var mkdirp = require("mkdirp");
var config_1 = require("./config");
function install(dir) {
    if (dir)
        config_1.settings.binpath = dir;
    log('installing into: ' + config_1.settings.binpath);
    mkdirp.sync(config_1.settings.binpath);
    download(link(), zipfile(), extractor());
}
exports.install = install;
function log() {
    var msg = [];
    for (var _i = 0; _i < arguments.length; _i++) {
        msg[_i] = arguments[_i];
    }
    console.log.apply(console, ['[ffmpeg]', '[' + config_1.settings.platform + ']'].concat(msg));
}
var extractors = {
    'win32': extractWin32,
    'darwin': extractDarwin,
};
function link() {
    return config_1.settings[config_1.settings.platform];
}
function zipfile() {
    return path.join(config_1.settings.binpath, 'temp.zip');
}
function extractor() {
    return extractors[config_1.settings.platform];
}
function download(url, dest, cb) {
    var file = fs.createWriteStream(dest);
    log('downloading...');
    http.get(url, function (response) {
        log('reading downloaded file');
        response.pipe(file);
        file.on('finish', function () {
            file.close();
            cb();
        });
    }).on('error', function (err) {
        remove(dest);
        log('error while downloading');
        if (cb)
            cb(err);
    });
}
function remove(path) {
    if (fs.existsSync(path))
        fs.unlinkSync(path);
}
function extractDarwin() {
    log('extracting executable binary');
    var zip = new AdmZip(zipfile());
    zip.extractEntryTo('ffmpeg', config_1.settings.binpath, false, true);
    fs.chmodSync(path.join(config_1.settings.binpath, 'ffmpeg'), '777');
    remove(zipfile());
    log('done');
}
function extractWin32() {
    log('extracting executable binary');
    var zip = new AdmZip(zipfile());
    var entries = zip.getEntries();
    var entry = entries[0].entryName;
    zip.extractEntryTo(entry + 'bin/ffmpeg.exe', config_1.settings.binpath, false, true);
    fs.chmodSync(path.join(config_1.settings.binpath, 'ffmpeg.exe'), '777');
    remove(zipfile());
    log('done');
}
