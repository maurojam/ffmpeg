/// <reference types="node" />
export declare const settings: any;
export declare function platform(): NodeJS.Platform;
export declare function setup(binpath: string): void;
export declare function bin(): string;
