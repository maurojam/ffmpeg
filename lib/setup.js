"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
var http = require("http");
var fs = require("fs");
var path = require("path");
var AdmZip = require("adm-zip");
var mkdirp = require("mkdirp");
exports.settings = {
    bindir: 'bin',
    platform: process.platform,
    win32: 'http://ffmpeg.zeranoe.com/builds/win32/static/ffmpeg-20170615-bc40674-win32-static.zip',
    darwin: 'http://www.ffmpegmac.net/resources/Lion_Mountain_Lion_Mavericks_Yosemite_El-Captain_15.05.2017.zip',
};
function setup(dir) {
    if (dir)
        exports.settings.bindir = dir;
}
exports.setup = setup;
function install() {
    mkdirp.sync(exports.settings.bindir);
    download(link(), zipfile(), extractor());
}
exports.install = install;
function bin() {
    return path.join(__dirname, path.join(exports.settings.bindir, executables[exports.settings.platform]));
}
exports.bin = bin;
var extractors = {
    'win32': extractWin32,
    'darwin': extractDarwin,
};
var executables = {
    'win32': 'ffmpeg.exe',
    'darwin': 'ffmpeg'
};
function link() {
    return exports.settings[exports.settings.platform];
}
function zipfile() {
    return path.join(exports.settings.bindir, 'temp.zip');
}
function extractor() {
    return extractors[exports.settings.platform];
}
function download(url, dest, cb) {
    var file = fs.createWriteStream(dest);
    http.get(url, function (response) {
        response.pipe(file);
        file.on('finish', function () {
            file.close();
            cb();
        });
    }).on('error', function (err) {
        remove(dest);
        if (cb)
            cb(err);
    });
}
function remove(path) {
    if (fs.existsSync(path))
        fs.unlinkSync(path);
}
function extractDarwin() {
    var zip = new AdmZip(zipfile());
    zip.extractEntryTo('ffmpeg', exports.settings.bindir, false, true);
    remove(zipfile());
}
function extractWin32() {
    var zip = new AdmZip(zipfile());
    var entries = zip.getEntries();
    var entry = entries[0].entryName;
    zip.extractEntryTo(entry + 'bin/ffmpeg.exe', exports.settings.bindir, false, true);
    remove(zipfile());
}
install();
