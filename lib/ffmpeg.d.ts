export interface RunResponse {
    cmd: string;
    code: number;
    failed: boolean;
    killed: boolean;
    signal: string | null;
    stderr: string;
    stdout: string;
    timedOut: boolean;
}
export declare function run(cmd: string): Promise<RunResponse>;
